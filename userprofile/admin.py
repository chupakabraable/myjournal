from django.contrib import admin
from models import UserProfile

# Register your models here.
class UserProfileAdmin(admin.ModelAdmin):

    list_display = ['user', 'nickname', 'gender']

admin.site.register(UserProfile, UserProfileAdmin)