from django.core.validators import MaxLengthValidator
from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UserProfile(models.Model):
    GENDER_MALE = 'M'
    GENDER_FEMALE = 'F'
    GENDER_NOT_SET = 'N'
    GENDER_CHOICES = (
        (GENDER_MALE, 'Male'),
        (GENDER_FEMALE, 'Female'),
        (GENDER_NOT_SET, 'not set'),
    )
    user = models.OneToOneField(User)
    nickname = models.CharField(max_length=50, unique=True)
    about = models.TextField(validators=[MaxLengthValidator(1000)], null=True, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, default=GENDER_NOT_SET)