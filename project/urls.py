from django.conf.urls import patterns, include, url
from django.contrib import admin
from blog.views import post_list, post_details
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    url(r'^$', post_list, name='post_list'),
    url(r'^post/(?P<post_id>\d{1,9})/$', post_details, name='post_details'),
    url(r'^admin/', include(admin.site.urls)),
)
