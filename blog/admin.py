from django.contrib import admin
from models import Post, Comment

# Register your models here.
class PostAdmin(admin.ModelAdmin):

    def secure_title(self):
        return self.title if self.title else '(no title)'

    secure_title.short_description = 'title'

    list_display = [secure_title, 'author', 'is_published', 'created_date']

    list_filter = ['created_date']

    search_fields = ['title']

admin.site.register(Post, PostAdmin)

class CommentAdmin(admin.ModelAdmin):

    list_display = ['author', 'text', 'created_date', 'post']

admin.site.register(Comment, CommentAdmin)


