from django.core.urlresolvers import reverse
from django.db import models

# Create your models here.
class Post(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200, null=True, blank=True)
    text = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    is_published = models.BooleanField(default=False, help_text='turn this checkbox off if you want to hide this post temporarily')

    def __str__(self):
        return self.title if self.title else '(no title)'

    def get_absolute_url(self):
        return reverse('post_details', args=[self.id])

    class Meta:
        ordering = ['-created_date']
        verbose_name = 'cool blog post'
        verbose_name_plural = 'cool blog posts'

class Comment(models.Model):
    author = models.ForeignKey('auth.User')
    text = models.TextField(max_length=200)
    created_date = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, related_name="comments")