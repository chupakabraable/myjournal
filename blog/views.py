from django.shortcuts import render, get_object_or_404, render_to_response
from django.template import RequestContext
from .models import Post

# Create your views here.
def post_list(request):
    posts = Post.objects.filter(is_published=True).order_by('created_date')
    return render_to_response('blog/post_list.html',
                              {'posts': posts},
                              context_instance=RequestContext(request))

def post_details(request, post_id):
    post = get_object_or_404(Post, id=post_id)
    return render_to_response('blog/post_detail.html',
                              {'post': post},
                              context_instance=RequestContext(request))